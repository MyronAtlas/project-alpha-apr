from django.urls import path
from projects.views import show_projects, view_project, create_project

urlpatterns = [
    path("", show_projects, name="list_projects"),
    path("<int:id>/", view_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
