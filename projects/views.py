from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def show_projects(request):
    showprojects = Project.objects.filter(owner=request.user)
    context = {"projects": showprojects}
    return render(request, "projects/list.html", context)


@login_required
def view_project(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {"form": form}
    return render(request, "projects/create.html", context)
