from django import forms
from tasks.models import Task


class CreateTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "is_completed",
            "project",
            "assignee",
        ]
        exclude = ["is_completed"]
